from firedrake import *

mesh = UnitSquareMesh(64, 64)
V_dg = FunctionSpace(mesh, "DG", 1)
V_cg = FunctionSpace(mesh, "CG", 1)
V_u  = VectorFunctionSpace(mesh, "CG", 2)

x, y = SpatialCoordinate(mesh)
f = Constant(0.0)
kappa = Constant(0.0)
alpha = Constant(5.0)

u = interpolate( Constant(("-1.","-0.4")), V_u)
v   = TestFunction(V_dg)
phi = TrialFunction(V_dg)

n = FacetNormal(mesh)
h = CellSize(mesh)
h_avg = (h('+') + h('-'))/2
un = (dot(u, n) + abs(dot(u, n)))/2.0

a_int = dot(grad(v), kappa*grad(phi) - u*phi)*dx

a_fac = kappa*(alpha/h('+'))*dot(jump(v, n), jump(phi, n))*dS \
      - kappa*dot(avg(grad(v)), jump(phi, n))*dS \
      - kappa*dot(jump(v, n), avg(grad(phi)))*dS
        
a_vel = dot(jump(v), un('+')*phi('+') - un('-')*phi('-') )*dS  + dot(v, un*phi)*ds

a = a_int + a_fac + a_vel

L = v*f*dx

g = Expression("sin(pi*5.0*x[1])", degree=2)
boundary_ids = (1, 2, 3, 4)
bc = DirichletBC(V_dg, g, boundary_ids, "geometric")

phi_h = Function(V_dg)

A = assemble(a)
b = assemble(L)
bc.apply(A, b)

solve(A, phi_h.vector(), b)

up = project(phi_h, V=V_cg)

file = File("temperature.pvd")
file << up


